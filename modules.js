// The operating system module
const os = require('os');
console.log(os.platform());

// The file system module

// 1. Read file
const fs = require('fs');
fs.readFile('.docs/notes.txt', (err, data)=>{
    if(err) console.log(err);
    console.log("data " + data);
});

// 2. Write file
fs.writeFile('.docs/notes.txt', 'Hello Fortune', (err, data)=>{
    console.log("File written");
});

// 3. Creating a directory
if(!fs.existsSync('./assets')){
    fs.mkdir('./assets', (err)=>{
        if(err) console.log(err);
        console.log("Folder created");
    })
}else{
    fs.rmdir('./assets',(err)=>{
        if(err) console.log(err);
        console.log("Folder removed");
    })
}

// 4. Deleting files
if(fs.existsSync('.docs/deleteme.txt')){
    fs.unlink('./deleteme.txt', (err)=>{
        if (err) console.log(err);
        console.log("File deleted");
    })
}